var Animator = (function(){
	var _debugId = "Animator";

	var _fps = 60;
	var _animations = [];

	function loadImage(id, source){
		Debug.log(_debugId, "loadImage(" + id + ", " + source + ")");
		var element = new Image();
		element.id = id;

		for(var i = 0; i<_animations.length; i++){
			if(_animations[i].id == id){
				_animations[i].image = element;
			}
		}

		element.onload = function() {
			onLoadSuccess(this.id);
			this.onload = this.onreadystatechange = null;
		};

		element.onerror = function() {
			onLoadError(this.id);
			this.onload = this.onreadystatechange = null;
		};

		element.onreadystatechange = function() {

			if(this.readyState == 'complete' || this.readyState == "loaded") {
				onLoadSuccess(this.id);
				this.onload = this.onreadystatechange = null;
			}
		};

		element.src = source;
	}

	function onLoadSuccess(id) {
		Debug.log(_debugId, "onLoadSuccess(" + id + ")");

		for(var i = 0; i<_animations.length; i++) {
			if(_animations[i].id == id) {
				_animations[i].isLoaded = true;
				_animations[i].spriteWidth = _animations[i].image.width;
				_animations[i].spriteHeight = _animations[i].image.height;

				if(_animations[i].fn != null) {
					_animations[i].fn();
				}
			}
		}
	};

	function onLoadError(id) {
		Debug.log(_debugId, "onLoadError(" + id + ")");
	};

	function animate(obj){
		var element = document.getElementById(obj.div);

		if(obj.currentFrame < obj.totalFrames){

			obj.currentFrame++;

			if(obj.currentXPos >= (obj.spriteWidth-obj.frameWidth)){
				obj.currentXPos = 0;
				obj.currentYPos += obj.frameHeight;
			}else{
				obj.currentXPos += obj.frameWidth;
			}
		}else{
			obj.currentXPos = 0;
			obj.currentYPos = 0;

			if(obj.loop){
				obj.currentFrame = 1;
			}else{
				Debug.log(_debugId, obj.id + " animation done");
				obj.isRunning = false;
				clearInterval(obj.animationLoop);

				if(obj.fn != null){
					obj.fn();
				}

				if(obj.showLastFrame){
					element.style.display = "block";
					obj.showLastFrame = false;
				}else{
					element.style.display = "none";
				}

				return;
			}
		}

		//Debug.log(_debugId,"animate(" + obj.id + " currentFrame: " + obj.currentFrame + ", xPos: " + obj.currentXPos + ", yPos: " + obj.currentYPos + ")");
		var image=element.getElementsByTagName("img")[0];
		image.style.left = -obj.currentXPos + "px";
		image.style.top = -obj.currentYPos + "px";

		if(obj.events.length > 0){
			if(obj.events[obj.currentFrame]){
				obj.events[obj.currentFrame]();
			}
		}
	}

	return {
		setFramesPerSecond:function(value){
			_fps = value;
		},
		getIsRunning:function(){
			var isRunning = false;

			for(var i = 0; i<_animations.length; i++) {
				if(_animations[i].isRunning){
					isRunning = true;
				}
			}

			return isRunning;
		},
		reset:function(){
			for(var i = 0; i<_animations.length; i++){
				var target = _animations[i];
				var element = document.getElementById(target.div);
				element.style.display = "none";
			}
		},
		addRollOver:function(id, divOn, sourceOn, framesOn, divOff, sourceOff, framesOff, width, height){
			Debug.log(_debugId, "addRollOver(" + id + ", " + divOn + ", " + framesOn + ", " + divOff + ", " + framesOff + ", " + width + ", " + height);
			Animator.addAnimation(divOn, divOn, sourceOn, width, height, framesOn, false);
			Animator.addAnimation(divOff, divOff, sourceOff, width, height, framesOff, false);
			var onElement = gwd.actions.events.getElementById(divOn);
			var offElement = gwd.actions.events.getElementById(divOff);
			var element = gwd.actions.events.getElementById(id);
			onElement.rollProps = offElement.rollProps = element.rollProps = {hit:element, id:id, divOn:divOn, framesOn:framesOn, divOff:divOff, framesOff:framesOff, width:width, height:height, over:false};
			Utils.addListener(element, "mouseover", handleMouseOver, false);
			Utils.addListener(element, "mouseout", handleMouseOut, false);
		},
		addAnimation:function(id, div, source, width, height, frames, loop, callback){
			Debug.log(_debugId, "addAnimation(" + id + ", " + div + ", " + source + ", " + width + ", " + height + ", " + frames + ", " + loop +  ")");

			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					Debug.log(_debugId, "animation " + id + " already exists");
					if(_animations[i].isLoaded){
						Debug.log(_debugId, "source already loaded..");
						if(callback){
							Debug.log(_debugId, "found callback..");
							callback();
						}
					}
					return;
				}
			}

			_animations.push({id:id, div:div, src:source, frameWidth:width, frameHeight:height, totalFrames:frames, loop:loop, fn:callback, isLoaded:false, events:[]});
			loadImage(id, source);
		},
		overwriteAnimation:function(id, source, width, height, frames){
			Debug.log(_debugId, "overwriteAnimation(" + id + ", " + source + ", " + width + ", " + height + ", " + frames  + ")");

			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					Debug.log(_debugId, "animation " + id + " found");
					_animations[i].source = source;
					_animations[i].width = width;
					_animations[i].height = height;
					_animations[i].frames = frames;
					loadImage(id, source);
					return;
				}
			}
		},
		addEvent:function(id, frame, callback){
			Debug.log(_debugId, "addEvent(" + id + ", " + frame + ", " + callback + ")");

			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					_animations[i].events[frame] = callback;
				}
			}
		},
		startAnimation:function(id, loop, callback, showLastFrame){
			Debug.log(_debugId, "startAnimation(" + id + ", " + loop + ")");
			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					_animations[i].currentFrame = 1;
					_animations[i].currentXPos = 0;
					_animations[i].currentYPos = 0;
					_animations[i].showLastFrame = showLastFrame;
					_animations[i].loop = loop;
					_animations[i].fn = callback;
					_animations[i].isRunning = true;
					var target = _animations[i];
					var element = document.getElementById(target.div);
					element.style.display = "block";
					var img = element.getElementsByTagName("img")[0];
					img.setAttribute('src', target.src);
					img.style.left = target.currentXPos + "px";
					img.style.top = target.currentYPos + "px";
					target.animationLoop = setInterval(function(){animate(target)}, (1000 / _fps));
				}
			}
		},
		stopAnimation:function(id) {
			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					var target = _animations[i];
					target.showLastFrame = false;
					target.isRunning = false;
					var element = document.getElementById(target.div);
					element.style.display = "none";
					clearInterval(_animations[i].animationLoop);
				}
			}
		},
		resetAnimation:function(id) {
			for(var i = 0; i<_animations.length; i++){
				if(_animations[i].id == id){
					var target = _animations[i];
					var element = document.getElementById(target.div);
					element.style.display = "none";
					var img = element.getElementsByTagName("img")[0];
					img.setAttribute('src', '');
					img.style.left = target.currentXPos + "px";
					img.style.top = target.currentYPos + "px";
				}
			}
		},
		getIsAnimationRunning:function(id){
			var isRunning = false;

			for(var i = 0; i<_animations.length; i++) {
				if(_animations[i].id == id){
					if(_animations[i].isRunning){
						isRunning = true;
					}
				}
			}

			return isRunning;
		},
		stopAll:function(){
			Debug.log(_debugId, "stopAll()");
			for(var i = 0; i<_animations.length; i++){
				var target = _animations[i];
				target.showLastFrame = false;
				target.isRunning = false;
				var element = document.getElementById(target.div);
				element.style.display = "none";
				clearInterval(_animations[i].animationLoop);
			}
		},
		numAnimations:function(){
			return _animations.length;
		}
	}
})();